# This is the take home assignment for Torch

## To run this service you need
* docker
* an API key from mta

This service contains three parts:
### API service which contains these endpoints:
An endpoint called /status, which takes the name of a particular line as an argument and returns whether or not the line is currently delayed.
An endpoint called /uptime, which also takes the name of a particular line as an argument and returns the fraction of time that it has not been delayed since server start time.

### Celery worker service that hits MTA api to get subway delays information
This is configured to run every 30 seconds

### Redis server for the celery workers and to hold subway delays data
This redis DB will hold the messaages required for celery to work and also to keep subway delays data as a short term persistent storage


## Instructions on how to run:
1. Clone this repository
2. Paste your API key in to the `API_KEY` field in docker-compose file
3. run `docker-compose up --build`
4. hit `localhost:8000/` to see a `{Health:check}` response
