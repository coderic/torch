import os
import time

from fastapi import FastAPI
import redis

app = FastAPI()

SERVER_START_TIME = time.time()
REDIS_URL = os.environ.get('BROKER_URL', 'redis://redis')
REDIS_PORT = os.environ.get('BROKER_PORT', '6379')
DELAYS_DB = os.environ.get('DELAYS_DB', 1)
memory_store = redis.Redis(host=f'{REDIS_URL}', port=REDIS_PORT, db=DELAYS_DB, decode_responses=True)
