from typing import Optional
import time

from config import app, memory_store, SERVER_START_TIME
import json
import redis


@app.get("/")
def root():
    return {"Health":"check"}

@app.get("/status/{line_id}")
def get_line_status(line_id: str):
    status = memory_store.lrange(line_id, -1, -1)
    if status:
        latest_status = json.loads(status[0])
        if 'delay_end' in latest_status:
            return {"Status": f"The {line_id} Line is currently not experiencing delays"}
        else:
            return {"Status": f"The {line_id} Line is currently experiencing delays"}
    else:
        return {"Status": f"The {line_id} Line is currently not experiencing delays"}


@app.get("/uptime/{line_id}")
def get_line_uptime(line_id: str):
    current_time = time.time()
    line_statuses = memory_store.lrange(line_id, 0, -1)
    downtime_start = 0
    total_downtime = 0
    total_uptime = 1
    for status in line_statuses:
        status = json.loads(status)
        # we only care about the times after the server started
        if list(status.values())[0] > SERVER_START_TIME:
            if 'delay_start' in status:
                downtime_start = status['delay_start']
            else:
                total_downtime = total_downtime + (status['delay_end'] - downtime_start)
                downtime_start = 0
    # this means there's still an ongoing downtime
    if downtime_start != 0:
        total_downtime = total_downtime + (current_time - downtime_start)
    # this block should cover the case where the latest delay_start
    # is before the server start time in which case we will just use the
    # server start time for when the delay started.
    if total_downtime == 0 and line_statuses:
        if 'delay_start' in line_statuses[-1]:
            last_value = json.loads(line_statuses[-1])
            total_downtime = current_time - SERVER_START_TIME
    total_uptime = 1 - (total_downtime / (current_time - SERVER_START_TIME))
    return {"uptime": total_uptime}
