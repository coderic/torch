from main import get_line_status, get_line_uptime
from fastapi.testclient import TestClient
from config import app

client = TestClient(app)

def test_get_line_status_returns_no_delay(mocker):
    line_id = 1
    delay_statuses = [
        "{\"delay_end\": 1646267678.5972662}",
    ]
    mocker.patch('main.memory_store.lrange', return_value=delay_statuses)
    response = client.get(f"/status/{line_id}")
    assert response.status_code == 200
    assert response.json() == {"Status": f"The {line_id} Line is currently not experiencing delays"}

def test_get_line_status_returns_delay(mocker):
    line_id = 1
    delay_statuses = [
        "{\"delay_start\": 1646267678.5972662}",
    ]
    mocker.patch('main.memory_store.lrange', return_value=delay_statuses)
    response = client.get(f"/status/{line_id}")
    assert response.status_code == 200
    assert response.json() == {"Status": f"The {line_id} Line is currently experiencing delays"}


def test_get_line_uptime_basic(mocker):
    line_id = 1
    mocker.patch('main.SERVER_START_TIME', 100)
    mocker.patch('main.time.time', return_value=300)
    delay_statuses = [
        "{\"delay_start\": 200}",
    ]
    mocker.patch('main.memory_store.lrange', return_value=delay_statuses)
    response = client.get(f"/uptime/{line_id}")
    assert response.status_code == 200
    assert response.json() == {"uptime": 0.5}

def test_get_line_uptime_no_downtime(mocker):
    line_id = 1
    mocker.patch('main.SERVER_START_TIME', 100)
    mocker.patch('main.time.time', return_value=300)
    delay_statuses = []
    mocker.patch('main.memory_store.lrange', return_value=delay_statuses)
    response = client.get(f"/uptime/{line_id}")
    assert response.status_code == 200
    assert response.json() == {"uptime": 1}

def test_get_line_uptime_shows_no_uptime_for_earlier_ongoing_delay(mocker):
    line_id = 1
    mocker.patch('main.SERVER_START_TIME', 400)
    mocker.patch('main.time.time', return_value=500)
    delay_statuses = [
        "{\"delay_start\": 200}",
    ]
    mocker.patch('main.memory_store.lrange', return_value=delay_statuses)
    response = client.get(f"/uptime/{line_id}")
    assert response.status_code == 200
    assert response.json() == {"uptime": 0.0}
