import os
from celery.schedules import crontab


redis_url = os.environ.get('BROKER_URL', 'redis://localhost')
redis_port = os.environ.get('BROKER_PORT', '6379')
broker_url = f'{redis_url}:{redis_port}'
task_serializer = 'json'
result_serializer = 'json'
accept_content = ['json']
timezone = 'Europe/Dublin'
enable_utc = True

CELERY_BEAT_SCHEDULE = {
    'every-minute': {
        'task': 'tasks.test',
        'schedule': 5
    }
}
