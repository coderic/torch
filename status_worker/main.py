from celery import Celery
import os
import redis


REDIS_URL = os.environ.get('BROKER_URL', 'redis://redis')
REDIS_PORT = os.environ.get('BROKER_PORT', '6379')

memory_store = redis.Redis(host='redis', port=6379, db=1, decode_responses=True)
api_key = os.environ.get('API_KEY', None)

app = Celery('foo', include=['tasks'])
app.config_from_object('celeryconfig')


app.conf.beat_schedule = {
    'add-every-30-seconds': {
        'task': 'tasks.set_subway_delays',
        'schedule': 30.0,
    },
}

