import requests
import json
import time
import main
from main import app, memory_store, api_key
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

"""
sample `delays` data in routes_dict:
"""

def get_service_delays():
    delays_dict = {}
    json_feed_url = 'https://api-endpoint.mta.info/Dataservice/mtagtfsfeeds/camsys%2Fsubway-alerts.json'
    headers = {'Content-Type': 'application/json', 'x-api-key': api_key}
    response_json = requests.get(json_feed_url, headers=headers)
    feed_data_json = json.loads(response_json.content)['entity']
    for alert in feed_data_json:
        routes = alert['alert']['informed_entity']
        for route in routes:
            route_id = route.get('route_id')
            alert_type = alert['alert']['transit_realtime.mercury_alert']['alert_type']
            if route_id and alert_type == 'Delays':
                delay_data = {"created_at": alert["alert"]["transit_realtime.mercury_alert"]["created_at"]}
                if route_id not in delays_dict:
                    delays_dict[route_id] = [delay_data]
                else:
                    delays_dict[route_id].append(delay_data)
    return delays_dict


def get_redis_delays_data():
    # get the redis delay data
    existing_delays_data = {}
    redis_keys = memory_store.keys("*")
    for key in redis_keys:
        existing_delays_data[key] = memory_store.lrange(key, -1, -1)
    return existing_delays_data


@app.task
def set_subway_delays():
    # get the delay data
    new_delay_data = get_service_delays()
    existing_delays_data = get_redis_delays_data()

    # get the difference
    difference_delays = set()
    # if key in existing does not show up in new data then existing delay is ended
    for key in existing_delays_data.keys():
        if key not in new_delay_data.keys():
            difference_delays.add(key)

    # push delays in to memory store because there are either still delays
    # or there are new delays
    _set_delay_starts(new_delay_data, existing_delays_data)

    # iterate through the difference and set the delay ends
    _set_delay_ends(difference_delays, existing_delays_data)

def _set_delay_starts(new_delay_data, existing_delays):
    # push delays in to memory store because there are either still delays
    # or there are new delays
    for k, v in new_delay_data.items():
        # if exists, we want to make sure we only insert a new delay
        # if the last entry was a delay_end
        if k in existing_delays:
            last_value = existing_delays[k][-1]
            if 'delay_end' in last_value:
                logger.info(f'Line {k} is experiencing delays')
                start_delay_object = {"delay_start": v[0]['created_at']}
                memory_store.rpush(k, json.dumps(start_delay_object))
        else:
            start_delay_object = {"delay_start": v[0]['created_at']}
            memory_store.rpush(k, json.dumps(start_delay_object))


def _set_delay_ends(difference_delays, existing_delays):
    for k in difference_delays:
        last_value = existing_delays[k][-1]
        if 'delay_start' in last_value:
            logger.info(f'Line {k} is now recovered')
            end_delay_object = {"delay_end": time.time()}
            memory_store.rpush(k, json.dumps(end_delay_object))



if __name__ == '__main__':
    get_service_delays()
