from unittest.mock import call
from tasks import get_service_delays, set_subway_delays, get_redis_delays_data
from snapshot import test_data


def test_get_service_delays(mocker):
    mocker.patch('requests.get')
    mocker.patch('json.loads', return_value=test_data)
    res = get_service_delays()
    assert res == {'Q': [{'created_at': 1646191671}], 'D': [{'created_at': 1646192960}], 'G': [{'created_at': 1646193109}]}


def test_set_subway_delays(mocker):
    mocked_time = 100
    logger_mock = mocker.patch('tasks.logger.info')
    mocker.patch('tasks.requests')
    mocker.patch('tasks.time.time', return_value=mocked_time)
    new_delay_data = {'Q': [{'created_at': 1646191671}], 'D': [{'created_at': 1646192960}], 'G': [{'created_at': 1646193109}]}
    mocker.patch('tasks.get_service_delays', return_value=new_delay_data)
    redis_delays_data = {
        'Q': [{'delay_start': 1646191371}, {'delay_end': 1646191471}],
        'D': [{'delay_start': 1646192460}],
        'G': [{'delay_start': 1646191109}],
        '1': [{'delay_start': 1646194109}, {'delay_end': 1646194509}],
        'M': [{'delay_start': 1646190109}]
    }
    mocker.patch('tasks.get_redis_delays_data', return_value=redis_delays_data)
    rpush_mock = mocker.patch('tasks.memory_store.rpush')
    set_subway_delays()
    rpush_call_list = rpush_mock.call_args_list
    expected_add_delay_start = call('Q', '{"delay_start": 1646191671}')
    expected_add_delay_ends = call('M', '{"delay_end": '+str(mocked_time)+'}')

    assert rpush_call_list[0] == expected_add_delay_start
    assert rpush_call_list[1] == expected_add_delay_ends
    logger_call_list = logger_mock.call_args_list
    expected_log_calls = [
        call(f'Line Q is experiencing delays'),
        call(f'Line M is now recovered')
    ]
    assert logger_call_list[0] in expected_log_calls
    assert logger_call_list[1] in expected_log_calls


def test_set_subway_delays_no_multiple_delay_ends(mocker):
    """Ensure that we don't insert multiple delay_end."""
    mocked_time = 100
    mocker.patch('tasks.requests')
    mocker.patch('tasks.time.time', return_value=mocked_time)
    new_delay_data = {'D': [{'created_at': 1646192960}], 'G': [{'created_at': 1646193109}]}
    mocker.patch('tasks.get_service_delays', return_value=new_delay_data)
    redis_delays_data = {
        'Q': [{'delay_start': 1646191371}, {'delay_end': 1646191471}],
        'D': [{'delay_start': 1646192460}],
        'G': [{'delay_start': 1646191109}],
        '1': [{'delay_start': 1646194109}, {'delay_end': 1646194509}],
    }
    mocker.patch('tasks.get_redis_delays_data', return_value=redis_delays_data)
    rpush_mock = mocker.patch('tasks.memory_store.rpush')
    set_subway_delays()
    rpush_mock.assert_not_called()
